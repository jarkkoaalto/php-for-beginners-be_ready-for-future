<?php

$color = "red";
// echo "$color <br>";
// echo $color'; ## Tämä tulostaa vain $color
$fname = "Jarkko";
$lname = "Aalto";
$combine = $fname . " " . $lname;
// Voidaan käyttää myös
// $combine = $fname;
// $combine .= $lname;

echo $combine;
echo ucwords($combine);  // Ensimmäinen pienellä
echo ucfirst($combine); // Ensimmäinen isolla
echo strtoupper($combine);
echo strtolower($combine);
?>