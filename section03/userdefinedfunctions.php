<?php

function welcome(){
	echo "You are nearly at the end of PHP course so 
	enroll now to any of my other course";
}

welcome();
echo "<br>";

function plus(){
	$a = 3;
	$b = 5;
	$c = $a + $b;
	echo $c;
}
plus();

echo "<br>";

function lisa($x,$y) {
	$u = $x + $y;
	return $u;
}

$u = lisa(5,6);
echo $u . "<br>";

function normal() {
	$value = 1;
	echo $value . "<br>";
	$value++;
}

normal();
normal();
normal();

function staticf() {
	static $valuee = 1;
	echo $valuee . "<br>";
	$valuee++;
	
}

staticf();
staticf();
staticf();
?>